# Answer Set Programming

[![clingo](https://img.shields.io/badge/clingo-v5.4.0-orange)](https://github.com/potassco/clingo/releases/)
[![KaTeX](https://img.shields.io/badge/using-KaTeX-9cf)](https://katex.org/)
[![MIT](https://img.shields.io/badge/license-MIT-brightgreen)](./LICENSE)

`Answer set programming` is a form of declarative programming that describes
viable solutions and the conditions with which arriving at them is valid. This
form of programming has its roots in _Artificial Intelligence_ and
_Computational Logic_.

## Declarative v. Imperative

Declarative languages do not encode an algorithm like a traditional imperative
language might. It is helpful to compare sentences in natural language to
demonstrate some of the key differences between the paradigms:

> An imperative sentence is a command that can be obeyed or disobeyed:
>
> "Go to school."
>
> A declarative sentence is a command that is `true` or `false`:
>
> "I am at school."

Declarative syntax in imperative programs:

> An imperative language is comprised of commands:
>
> "multiply `n` by `2`."
>
> And those commands can be modified by declarative constructs, such as
> `equalities` and `inequalaties`:
>
> "multiply `n` by `2` until  `m > n`"

Declarative languages have conditions on the values of variables that
characterize solutions to a problem. Assignments do not exist.

## Logic Programming

`Logic Programs` are comprised of `rules`. A `rule` consists of a `heaad` and
a `body`:

```clingo
large(C) :- size(C, S1), size(uk, S2), S1 > S2.
```

The head is:

```clingo
large(C)
```

The bod is:

```clingo
size(C, S1), size(uk, S2), S1 > S2
```

Things of note:

- `rules` end with `.`
- The `:-` separates the `head` and `body` and is read as `if`
- Commas in the `body` are _conjuctions_ and are read as `and`
- Capitalized identifiers are *variables* (e.g. `C`, $`S_1`$, $`S_2`$)
- specific objects are not capitalized (e.g. `uk`)
- Items with parenthesis are _binary relations_ (e.g. `size()`)
  - In this specific instance, $`size(C, S_n)`$ is the _binary relation_ between a county, `C` and its population size $`S_n`$

Putting it all together we can read the above `rule` as:

> A country `C` is large `if` the population size of `C` is $`S_1`$, the population size of the UK is $`S_2`$, and $`S_1 > S_2`$

### Helper directives

Obviously there are comments:

```clingo
% This is a comment
```

#### show

`#show` will display some elements of the stable model and suppress the others.

```clingo
#show large/1.
```

Here, `/n` refers to the arity of the predicate symbol:

```clingo
p. p(a). p(a,b).

# show p/0. #show p/2.
```

#### const

The `#const` directive allows us to use a symbollic constant as a placeholder:

```clingo
large(C) :- size(C,S1), size(uk,S2), S1 > S2.

#const c0=uk.
large(C) :- size(C,S1), size(c0,S2), S1 > S2.
```

This can also be done on the command line via:

```sh
clingo some.lp -c c0=uk
```

#### include

You can add multiple files. For instance, we could split input and rules:

```sh
clingo large.lp large_input.lp
```

Alternatively, you can use the `#include` directive inside of a file:

```clingo
#include "large_input.lp"
```

### Arithmetic

Complex terms can be built from constants and variables using the arithmetic symbols.

```clingo
p(N,N*N+N+41) :- N=0..3.
```

Which is read as: $`N and N^2 + N + 41 are in the relation p/2 if N is one of the numbers 0, ..., 3`$

#### Composite numbers

An integer `N` is composite if it is divisible by some numbers from $`{2, ..., N-1}`$

```clingo
composite(N) :- N=1..n, I=2..N-1, N\I=0.
```

### Framing rules

Continuing with the above example we can codify a table of data into additional rules:

| Country               | France | Germany | Italy | United Kingdom |
| --------------------- | ------ | ------- | ----- | -------------- |
| Population (Millions) | 65     | 83      | 61    | 64             |

Becomes:

```clingo
size(france,65).
size(germany,83).
size(italy,61).
size(uk,64).
```

Additionally, we can derive more facts of the data as `rules`:

```clingo
large(france).
large(germany).
```

## Problem types

### Combinatorial

### Graphs

Graphs can be `directed` or `undirected`. These are commonly used to

#### Cliques

Clique models idealize three important structural properties that are expected
of a cohesive subgroup:

- `familiarity`: each vertex has many neighbors and only a few strangers
- `reachability`: a low diameter, facilitating fast communication between members
- `robustness`: high connectivity, making it difficult to detroy the group by removing members

Applications:

- model `social networks`
- patterns in `telecommunications traffic`
- `bioinformatics` to identify common substructures between a collection of molecules known to possess certain pharmacological properties
- fault diagnosis on large `multiprocessor systems`
- `computer vision` and `pattern recognition`

#### Vertex cover

A `vertex cover` of a graph is a set of vertices such that every edge of the
graph has at least one end point in that set. Finding the minimum size for
vertex cover is the interesting problem.

#### Hamiltonian Cycles

This is most commonly called the `Travelling Salesman` problem.

A `hamiltonian cycle` in a graph `G` is a subgraph `C` of `G` that has the
same set `V` of vertices as `G` and satisfies two conditions:

- Every vertex has only one incoming edge and only one outgoing edge in the cycle
- Every vertex is reachable in `C` from some fixed vertex (say, 1)

### Elaboration Tolerance

This is used for `combinatorial research problems` and describes how to
encode their information in `ASP`.

> A formalism is `elaboration tolerant` to the extend that it is convenient
> to modify a set of facts expressed in the formalism to take into account new
> phenomena or changed circumstances. - McCarthy 2003

> Representations of information in natural language have good elaboration
> tolerance when used with human background knowledge. - McCarthy 2003

> Human-level AI will require representations with much more elaboration
> tolerance than those used by present AI programs, because human-level AI
> needs to be able to take new phenomena into account - McCarthy 2003

#### Variations of Sudoku

- `Offset`: A region is represented by the same color. In addition to the requirement of sudoku, every region must contain all the digits `1` through `9`.
- `Anti-Knight`: Cells that are a chess knight's move away from each other cannot hold equal values.
- `Greater-Than`: no numerical clues; only greater-than $`>`$ relationships

## Optimizations and Aggregates

An `aggregate` is a function that can be applied to sets, such as `#count`, `#sum`, `#min`, and `#max`.

The part of an aggregate expression to the left of the colon may include not only variables, but also more complex terms.

## Resources and References

The following resources are primarily where I derived exercises and notes from.

Sites:

- [clingo getting started](https://potassco.org/doc/start/)
- [clingo repository](https://github.com/potassco/clingo)
- [potassco guide](https://github.com/potassco/guide/releases/)

Books:

- [An Introduction to Description Logic](https://smile.amazon.com/Introduction-Description-Logic-Franz-Baader/dp/0521873614/ref=smi_www_rco2_go_smi_4368549507?_encoding=UTF8&%2AVersion%2A=1&%2Aentries%2A=0&ie=UTF8)
- [Answer Set Programming](https://smile.amazon.com/Answer-Set-Programming-Vladimir-Lifschitz/dp/3030246574/ref=sr_1_2?dchild=1&keywords=answer+set+programming&qid=1610329308&s=books&sr=1-2)
- [Answer Set Solving in Practice](https://smile.amazon.com/Practice-Synthesis-Lectures-Artificial-Intelligence/dp/1608459713/ref=sr_1_1?dchild=1&keywords=answer+set+solving+in+practice&qid=1610329337&s=books&sr=1-1)
