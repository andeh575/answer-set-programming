% Research papers submitted to a technical conference are usuall reviewed
% by the conference program committee. Every submission is read and
% discussed by a group of committee members chosen by the chair of
% the committee, and this group decides if the paper can be accepted
% for presentation.

% To help the chair find a good match between submissions and referees,
% every committee member submits a bid that classifies all submissions
% into three categories:
%
% "YES" - I want to review this submission
% "MAYBE" - I don't mind reviewing it
% "NO" - Do not assign it to me
%
% The chair tries to assign each submission for review to a specific
% number k of comittee members so that:
% - workloads are approximately equal
% - no committee member is asked to review a submission they said 'no' to
% - the total number of cases when a submission is assigned to a reviewer
%   who placed it in the 'yes' group is as large as possible

% input:
%
% k: number of referees per submission
% referee/1: set of committee members
% submission/1: set of submissions
% bid/3: set of triples (R,S,B) such that B is the R's bid for submission S

% every submission is assigned to k referees who don't mind reviewing it
{review(R,S): referee(R), not bid(R,S,no)} = k :- submission(S).

% workload(R,N) iff N is the number of submissions assigned to referee R
workload(R,N) :- referee(R), N = #count{S: review(R,S)}.

% difference between the workloads of the committee members is at most 1
:- workload(R1,N1), workload(R2,N2), |N1-N2| > 1.

% number of 'yes' cases is maximal
#maximize{1,R,S: bid(R,S,yes), review(R,S)}.
