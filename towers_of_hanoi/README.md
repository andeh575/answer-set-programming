# Towers of Hanoi

The `Towers of Hanoi` puzzle consists of three pegs and four disks of various
sizes. The goal of the puzzle is to move all disks from the left-most peg to
the right-most peg. Only the topmost disk can be moved on any given peg.
Additionally, a disk cannot be moved to peg already containing a disk that is
smaller.

There exists an efficient algorithm for solving the puzzle but we do not exploit
it here - we merely specify conditions for sequences of moves being solutions.

## Process

### Uniform problem definition

Given an initial placement of the disks, a goal situation, and a number `n`,
decide whether there is a sequence of `n` moved that achieves the goal.

The initial configuration, goal state, and conditions of the problem are
described in an `instance`.

### Problem encoding

The problem is encoded via `schematic rules`, which are rules containing
variables that are independent of a *particular instance*. Typically, an
encoding can be partitioned into a `generate`, `define`, and `test` parts.
Sometimes a `display` part allows for restricting the output to a distinguished
set of `atoms`, and thus, for suppressing auxiliary predicates.

## Execution

```sh
clingo instance.lp encoding.lp
```
